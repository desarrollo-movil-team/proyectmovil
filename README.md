# App-Notas

    Aplicación que permite subir fotos de manera rapida, simple y eficaz

# Integrantes

    NOE AYALA CERDA
    JESÚS MACÍN ROSALES
    VERÓNICA LORENZO ALAVEZ
    VÍCTOR HUGO AGUILAR ÁGUILA
    LUIS HUMBERTO VARGAS SÁNCHEZ
    RICARDO ALEXIS MENDOZA CHÁVEZ

## Intall

    ´´´npm
    npm start
    ´´´

# Usage

    ´´´bash
    npm start
    ´´´

# Contributing

    Proyecto Creado a modo laboratorio para la clase Desarrollo Móvil

# License

    ProyectMovil is released under the [MIT Licence](https://opensource.org/license/MIT).
